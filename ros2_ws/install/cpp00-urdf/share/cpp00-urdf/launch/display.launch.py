from launch import LaunchDescription
from launch_ros.actions import Node

from launch_ros.parameter_descriptions import ParameterValue
from launch.substitutions import Command

from ament_index_python.packages import get_package_share_directory

from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration

import os

def generate_launch_description():

    model = DeclareLaunchArgument(name="model", default_value=get_package_share_directory("cpp00-urdf") + "/urdf/urdf/cat259d.urdf")
    # p_value = ParameterValue(Command(["xacro ",get_package_share_directory("cpp00-urdf") + "/urdf/urdf/cat259d.urdf"]))
    p_value = ParameterValue(Command(["xacro ",LaunchConfiguration("model")]))

    # config = os.path.join(
    #     get_package_share_directory('cpp00-urdf'),
    #     'config',
    #     'move.yaml'
    # )

#     controller_manager_node = Node(
#     package='controller_manager',
#     executable='ros2_control_node',
#     # parameters=[config],
#     output={
#         'stdout': 'screen',
#         'stderr': 'screen',
#     },
# )



    controller_node = Node(
        package='move_control',
        executable='base_link_controller_node',
        output={
            'stdout': 'screen',
            'stderr': 'screen',
        },
    )

    arm_controller_node = Node(
        package='move_control',
        executable='arm_controller_node',
        output={
            'stdout': 'screen',
            'stderr': 'screen',
        },
    )

    bucket_controller_node = Node(
        package='move_control',
        executable='bucket_controller_node',
        output={
            'stdout': 'screen',
            'stderr': 'screen',
        },
    )

    robot_state_pub = Node(
        package="robot_state_publisher",
        executable="robot_state_publisher",
        parameters=[{"robot_description":p_value}]
    )

    # joint_state_pub = Node(
    #     package="joint_state_publisher",
    #     executable="joint_state_publisher"
    # )

    # joint_state_pub_gui = Node(
    #     package="joint_state_publisher_gui",
    #     executable="joint_state_publisher_gui"
    # )

    rviz2 = Node(
        package="rviz2", 
        executable="rviz2",
        arguments=["-d", get_package_share_directory("cpp00-urdf") + "/rviz/urdf.rviz"]
    )
    return LaunchDescription([controller_node, model, robot_state_pub, arm_controller_node, bucket_controller_node, rviz2])