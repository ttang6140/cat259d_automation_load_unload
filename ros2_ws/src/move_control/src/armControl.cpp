#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/float64.hpp>
#include <geometry_msgs/msg/twist.hpp>

class ArmStatePublisher : public rclcpp::Node
{
public:
    ArmStatePublisher() : Node("arm_controller"), current_position(0.0)
    {
        joint_state_publisher_ = this->create_publisher<sensor_msgs::msg::JointState>("/joint_states", 10);
        arm_command_subscription_ = this->create_subscription<std_msgs::msg::Float64>(
            "arm_command", 10, std::bind(&ArmStatePublisher::arm_command_callback, this, std::placeholders::_1));

        sync_tf_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);

        // 定期发布当前关节状态
        timer_ = this->create_wall_timer(
            std::chrono::milliseconds(100),  // 以100毫秒为周期
            std::bind(&ArmStatePublisher::publish_current_state, this));
    }

private:
    void arm_command_callback(const std_msgs::msg::Float64::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "Received arm command: %f", msg->data);
        current_position = msg->data;  // 更新当前位置
    }

    void publish_current_state()
    {
        sensor_msgs::msg::JointState joint_state_msg;
        joint_state_msg.header.stamp = this->now();
        joint_state_msg.name.push_back("base_arm");
        joint_state_msg.position.push_back(current_position);

        joint_state_publisher_->publish(joint_state_msg);

        //同步tf
        auto message = geometry_msgs::msg::Twist();
        message.linear.x = 0.0;
        message.linear.y = 0.0;
        message.linear.z = 0.0;
        message.angular.x = 0.0;
        message.angular.y = 0.0;
        message.angular.z = 0.0;

        // RCLCPP_INFO(this->get_logger(), "Publishing: '%f'", message.linear.x);
        sync_tf_publisher_->publish(message);
    }

    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr joint_state_publisher_;
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr arm_command_subscription_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr sync_tf_publisher_;
    rclcpp::TimerBase::SharedPtr timer_;

    double current_position;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<ArmStatePublisher>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
