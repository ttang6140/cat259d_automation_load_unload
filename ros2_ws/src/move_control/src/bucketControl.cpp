#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/float64.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>

class BucketStatePublisher : public rclcpp::Node
{
public:
    BucketStatePublisher() : Node("bucket_controller"), current_position(0.0)
    {
        joint_state_publisher_ = this->create_publisher<sensor_msgs::msg::JointState>("/joint_states", 10);
        bucket_command_subscription_ = this->create_subscription<std_msgs::msg::Float64>(
            "bucket_command", 10, std::bind(&BucketStatePublisher::bucket_command_callback, this, std::placeholders::_1));

        timer_ = this->create_wall_timer(
            std::chrono::milliseconds(100),
            std::bind(&BucketStatePublisher::publish_current_state, this));

        sync_tf_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);

        tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(*this);
        position_subscription_ = this->create_subscription<geometry_msgs::msg::Pose>(
            "model_updated", 10, std::bind(&BucketStatePublisher::position_callback, this, std::placeholders::_1));
    }

private:
    void bucket_command_callback(const std_msgs::msg::Float64::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "Received bucket command: %f", msg->data);
        current_position = msg->data;
    }

    void position_callback(const geometry_msgs::msg::Pose::SharedPtr msg)
    {

        // RCLCPP_INFO(this->get_logger(), "Received Position: X: %f, Y: %f", msg->position.x, msg->position.y);
        // RCLCPP_INFO(this->get_logger(), "Received Orientation: (%f, %f, %f, %f)",
        //             msg->orientation.x, msg->orientation.y,
        //             msg->orientation.z, msg->orientation.w);

        // 使用接收到的位置信息来更新 TF 变换
        geometry_msgs::msg::TransformStamped transformStamped;
        transformStamped.header.stamp = this->now();
        transformStamped.header.frame_id = "world";
        transformStamped.child_frame_id = "base_link";
        transformStamped.transform.translation.x = msg->position.x;
        transformStamped.transform.translation.y = msg->position.y;
        transformStamped.transform.translation.z = 0.0;
        transformStamped.transform.rotation = msg->orientation;

        tf_broadcaster_->sendTransform(transformStamped);
    }

    void publish_current_state()
    {
        sensor_msgs::msg::JointState joint_state_msg;
        joint_state_msg.header.stamp = this->now();
        joint_state_msg.name.push_back("arm_bucket");
        joint_state_msg.position.push_back(current_position);

        // 打印 joint_state_msg 的内容
        // RCLCPP_INFO(this->get_logger(), "Publishing Joint State:");
        // RCLCPP_INFO(this->get_logger(), "  Header Stamp: %ld.%ld", joint_state_msg.header.stamp.sec, joint_state_msg.header.stamp.nanosec);
        // RCLCPP_INFO(this->get_logger(), "  Joint Name: %s", joint_state_msg.name[0].c_str());
        // RCLCPP_INFO(this->get_logger(), "  Joint Position: %f", joint_state_msg.position[0]);

        joint_state_publisher_->publish(joint_state_msg);

        //同步tf
        auto message = geometry_msgs::msg::Twist();
        message.linear.x = 0.0;
        message.linear.y = 0.0;
        message.linear.z = 0.0;
        message.angular.x = 0.0;
        message.angular.y = 0.0;
        message.angular.z = 0.0;

        // RCLCPP_INFO(this->get_logger(), "Publishing: '%f'", message.linear.x);
        sync_tf_publisher_->publish(message);
    }

    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr joint_state_publisher_;
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr bucket_command_subscription_;
    rclcpp::Subscription<geometry_msgs::msg::Pose>::SharedPtr position_subscription_;
    std::unique_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr sync_tf_publisher_;
    rclcpp::TimerBase::SharedPtr timer_;

    double current_position;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<BucketStatePublisher>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
