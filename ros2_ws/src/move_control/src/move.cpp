#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <geometry_msgs/msg/pose.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2/LinearMath/Matrix3x3.h>
#include <std_msgs/msg/float64.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <std_msgs/msg/bool.hpp>

class BaseLinkController : public rclcpp::Node
{

    // 装货过程的状态
    enum class LoadingState
    {
        IDLE,
        MOVING_ARM_1,
        MOVING_BUCKET,
        MOVING_FORWARD,
        MOVING_ARM_2,
        COMPLETED
    };

    LoadingState loading_state_ = LoadingState::IDLE;
    rclcpp::TimerBase::SharedPtr loading_timer_;

    // 卸货过程的状态
    enum class UnloadingState
    {
        IDLE,
        MOVING_FORWARD,
        MOVING_ARM,
        MOVING_BUCKET,
        COMPLETED
    };

    UnloadingState unloading_state_ = UnloadingState::IDLE;
    rclcpp::TimerBase::SharedPtr unloading_timer_;

    rclcpp::Time last_time_;
    double x_, y_, theta_;

    rclcpp::Subscription<geometry_msgs::msg::Pose>::SharedPtr target_position_subscription_;

    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_publisher_;

    // rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr joint_state_subscription_;

    // rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr processed_joint_state_publisher_;

    // 发布状态变化的通知
    rclcpp::Publisher<geometry_msgs::msg::Pose>::SharedPtr model_update_publisher_;

    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr load_unload_subscription_;

    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr arm_publisher_;
    rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr bucket_publisher_;
    rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr forward_publisher_;

public:
    BaseLinkController() : Node("base_link_controller"), last_time_(this->now()), x_(0.0), y_(0.0), theta_(0.0)
    {
        RCLCPP_INFO(this->get_logger(), "Position: (%f, %f), Orientation: %f", x_, y_, theta_);
        subscription_ = this->create_subscription<geometry_msgs::msg::Twist>(
            "cmd_vel", 10, std::bind(&BaseLinkController::cmd_vel_callback, this, std::placeholders::_1));

        target_position_subscription_ = this->create_subscription<geometry_msgs::msg::Pose>(
            "target_position", 10, std::bind(&BaseLinkController::target_position_callback, this, std::placeholders::_1));

        cmd_vel_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

        // 初始化TF广播器
        tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(*this);

        // 初始化订阅者以接收装卸货更新的通知
        load_unload_subscription_ = this->create_subscription<std_msgs::msg::Bool>(
            "load_updated", 10, std::bind(&BaseLinkController::load_update_callback, this, std::placeholders::_1));

        arm_publisher_ = this->create_publisher<std_msgs::msg::Float64>("/arm_command", 10);
        bucket_publisher_ = this->create_publisher<std_msgs::msg::Float64>("/bucket_command", 10);
        forward_publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);

        // joint_state_subscription_ = this->create_subscription<sensor_msgs::msg::JointState>(
        //     "/joint_states", 10,
        //     std::bind(&BaseLinkController::joint_state_callback, this, std::placeholders::_1));

        // processed_joint_state_publisher_ = this->create_publisher<sensor_msgs::msg::JointState>("/joint_states", 10);

        // 初始化用于发布带有位置信息的模型更新通知的发布者
        model_update_publisher_ = this->create_publisher<geometry_msgs::msg::Pose>("model_updated", 10);
    }

private:
    void cmd_vel_callback(const geometry_msgs::msg::Twist::SharedPtr msg)
    {
        auto now = this->now();
        // double delta_t = (now - last_time_).seconds();
        double delta_t = 0.1;
        last_time_ = now;

        // 计算旋转后的速度分量
        double dx = msg->linear.x * cos(theta_) - msg->linear.y * sin(theta_);
        double dy = msg->linear.x * sin(theta_) + msg->linear.y * cos(theta_);

        x_ += dx * delta_t;
        y_ += dy * delta_t;
        theta_ += msg->angular.z * delta_t;

        // x_ += msg->linear.x * delta_t;
        // y_ += msg->linear.y * delta_t;
        // theta_ += msg->angular.z * delta_t;

        geometry_msgs::msg::TransformStamped transformStamped;
        transformStamped.header.stamp = now;
        transformStamped.header.frame_id = "world";
        transformStamped.child_frame_id = "base_link";
        transformStamped.transform.translation.x = x_;
        transformStamped.transform.translation.y = y_;
        transformStamped.transform.translation.z = 0.0;

        tf2::Quaternion q;
        q.setRPY(0, 0, theta_);
        transformStamped.transform.rotation.x = q.x();
        transformStamped.transform.rotation.y = q.y();
        transformStamped.transform.rotation.z = q.z();
        transformStamped.transform.rotation.w = q.w();

        tf_broadcaster_->sendTransform(transformStamped);

        // 构造并发布包含当前位置信息的消息
        geometry_msgs::msg::Pose update_msg;
        update_msg.position.x = x_;
        update_msg.position.y = y_;
        update_msg.orientation = tf2::toMsg(q);

        // 打印 update_msg 的内容
        // RCLCPP_INFO(this->get_logger(), "Publishing Update Message:");
        // RCLCPP_INFO(this->get_logger(), "  Position X: %f, Y: %f", update_msg.position.x, update_msg.position.y);
        // RCLCPP_INFO(this->get_logger(), "  Orientation: (%f, %f, %f, %f)",
        //             update_msg.orientation.x, update_msg.orientation.y,
        //             update_msg.orientation.z, update_msg.orientation.w);

        model_update_publisher_->publish(update_msg);
    }

    void target_position_callback(const geometry_msgs::msg::Pose::SharedPtr msg)
    {
        // 提取目标位置
        double target_x = msg->position.x;
        double target_y = msg->position.y;

        // 计算到目标位置的距离
        double distance_to_target = sqrt(pow(target_x - x_, 2) + pow(target_y - y_, 2));

        // 转换四元数到欧拉角
        tf2::Quaternion q(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
        tf2::Matrix3x3 m(q);
        double roll, pitch, yaw;
        m.getRPY(roll, pitch, yaw);

        // 计算角度差
        double angle_to_target = atan2(target_y - y_, target_x - x_);
        double angle_diff = angle_to_target - theta_;
        angle_diff = atan2(sin(angle_diff), cos(angle_diff)); // 规范化到 [-pi, pi]

        geometry_msgs::msg::Twist cmd_vel_msg;

        // 如果角度差足够小，且距离目标一定距离，则直线移动
        if (fabs(angle_diff) < 0.1 && distance_to_target > 1.0)
        {
            cmd_vel_msg.linear.x = std::min(distance_to_target, 0.5); // 限制最大速度
            cmd_vel_msg.angular.z = 0.0;
        }
        // 否则，旋转以面向目标方向
        else if (fabs(angle_diff) >= 0.1)
        {
            cmd_vel_msg.angular.z = std::min(std::max(angle_diff, -0.5), 0.5); // 限制最大旋转速度
            cmd_vel_msg.linear.x = 0.0;
        }
        // 如果已经到达目标附近，则停止
        else
        {
            // 如果距离目标足够近且操作尚未完成
            if (distance_to_target <= 1.0 && !operation_completed_)
            {
                if (load)
                {
                    // 执行装货操作
                    performLoading();
                }
                else
                {
                    // 执行卸货操作
                    performUnloading();
                }
                operation_completed_ = true; // 标记操作已完成
            }
            cmd_vel_msg.linear.x = 0.0;
            cmd_vel_msg.angular.z = 0.0;
        }

        // 发布控制命令
        cmd_vel_publisher_->publish(cmd_vel_msg);
    }

    void load_update_callback(const std_msgs::msg::Bool::SharedPtr msg)
    {

        load = msg->data;

        if (operation_completed_)
        {
            operation_completed_ = false;
        }
    }

    void performLoading()
    {
        // 执行装货动作
        loading_state_ = LoadingState::MOVING_ARM_1; // 设置初始状态

        // 创建定时器来逐步执行装货过程
        loading_timer_ = this->create_wall_timer(
            std::chrono::seconds(1), // 间隔时间，根据需要调整
            std::bind(&BaseLinkController::loadingStep, this));
    }

    void performUnloading()
    {
        // 执行卸货动作
        unloading_state_ = UnloadingState::MOVING_FORWARD; // 设置初始状态

        // 创建定时器来逐步执行卸货过程
        unloading_timer_ = this->create_wall_timer(
            std::chrono::seconds(1), // 间隔时间，根据需要调整
            std::bind(&BaseLinkController::unloadingStep, this));
    }

    // void joint_state_callback(const sensor_msgs::msg::JointState::SharedPtr msg)
    // {
    //     // 在这里处理接收到的关节状态
    //     sensor_msgs::msg::JointState processed_joint_state;
    //     processed_joint_state.header.stamp = this->now(); // 设置当前时间戳

    //     processed_joint_state.name = msg->name;
    //     processed_joint_state.position = msg->position;
    //     processed_joint_state.velocity = msg->velocity;
    //     processed_joint_state.effort = msg->effort;

    //     // 发布处理后的关节状态
    //     processed_joint_state_publisher_->publish(processed_joint_state);
    // }

    void unloadingStep()
    {
        switch (unloading_state_)
        {
        case UnloadingState::MOVING_FORWARD:
            moveForwardUnload();
            unloading_state_ = UnloadingState::MOVING_ARM;
            break;
        case UnloadingState::MOVING_ARM:
            moveArmUnload();
            unloading_state_ = UnloadingState::MOVING_BUCKET;
            break;
        case UnloadingState::MOVING_BUCKET:
            moveBucketUnload();
            unloading_state_ = UnloadingState::COMPLETED;
            break;
        case UnloadingState::COMPLETED:
            RCLCPP_INFO(this->get_logger(), "Unloading completed");
            unloading_timer_->cancel(); // 停止定时器
            break;
        default:
            break;
        }
    }

    void moveForwardUnload()
    {
        // 发布前进命令
        auto forward_message_unLoad = geometry_msgs::msg::Twist();
        forward_message_unLoad.linear.x = 0.5;
        forward_message_unLoad.linear.y = 0.0;
        forward_message_unLoad.linear.z = 0.0;
        forward_message_unLoad.angular.x = 0.0;
        forward_message_unLoad.angular.y = 0.0;
        forward_message_unLoad.angular.z = 0.0;
        forward_publisher_->publish(forward_message_unLoad);
    }

    void moveArmUnload()
    {
        // 发布移动机械臂的命令
        auto arm_msg_unLoad = std_msgs::msg::Float64();
        arm_msg_unLoad.data = -0.1;
        arm_publisher_->publish(arm_msg_unLoad);
    }

    void moveBucketUnload()
    {
        // 发布移动斗的命令
        auto bucket_msg_unLoad = std_msgs::msg::Float64();
        bucket_msg_unLoad.data = 0.59;
        bucket_publisher_->publish(bucket_msg_unLoad);
        RCLCPP_INFO(this->get_logger(), "Unloading completed");
    }

    void loadingStep() {
        switch (loading_state_) {
            case LoadingState::MOVING_ARM_1:
                moveArmLoad_1();
                loading_state_ = LoadingState::MOVING_BUCKET;
                break;
            case LoadingState::MOVING_BUCKET:
                moveBucketLoad();
                loading_state_ = LoadingState::MOVING_FORWARD;
                break;
            case LoadingState::MOVING_FORWARD:
                moveForwardLoad();
                loading_state_ = LoadingState::MOVING_ARM_2;
                break;
            case LoadingState::MOVING_ARM_2:
                moveArmLoad_2();
                loading_state_ = LoadingState::COMPLETED;
                break;
            case LoadingState::COMPLETED:
                RCLCPP_INFO(this->get_logger(), "Loading completed");
                loading_timer_->cancel(); // 停止定时器
                break;
            default:
                break;
        }
    }

    void moveForwardLoad()
    {
        // 发布前进命令
        auto forward_message_load = geometry_msgs::msg::Twist();
        forward_message_load.linear.x = 0.5;
        forward_message_load.linear.y = 0.0;
        forward_message_load.linear.z = 0.0;
        forward_message_load.angular.x = 0.0;
        forward_message_load.angular.y = 0.0;
        forward_message_load.angular.z = 0.0;
        forward_publisher_->publish(forward_message_load);
    }

    void moveArmLoad_1()
    {
        auto arm_msg_load_1 = std_msgs::msg::Float64();
        arm_msg_load_1.data = 0.0;
        arm_publisher_->publish(arm_msg_load_1);
    }

    void moveArmLoad_2()
    {
        auto arm_msg_load_2 = std_msgs::msg::Float64();
        arm_msg_load_2.data = -0.28;
        arm_publisher_->publish(arm_msg_load_2);
    }

    void moveBucketLoad()
    {
        auto bucket_msg_load = std_msgs::msg::Float64();
        bucket_msg_load.data = 0.39;
        bucket_publisher_->publish(bucket_msg_load);
    }

    double load;

    bool operation_completed_ = false;

    std::unique_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster_;

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscription_;
};

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<BaseLinkController>());
    rclcpp::shutdown();
    return 0;
}