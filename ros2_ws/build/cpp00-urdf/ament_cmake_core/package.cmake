set(_AMENT_PACKAGE_NAME "cpp00-urdf")
set(cpp00-urdf_VERSION "0.0.0")
set(cpp00-urdf_MAINTAINER "root <root@todo.todo>")
set(cpp00-urdf_BUILD_DEPENDS )
set(cpp00-urdf_BUILDTOOL_DEPENDS "ament_cmake")
set(cpp00-urdf_BUILD_EXPORT_DEPENDS )
set(cpp00-urdf_BUILDTOOL_EXPORT_DEPENDS )
set(cpp00-urdf_EXEC_DEPENDS "ros2launch" "xacro" "robot_state_publisher" "joint_state_publisher" "rviz2")
set(cpp00-urdf_TEST_DEPENDS "ament_lint_auto" "ament_lint_common")
set(cpp00-urdf_GROUP_DEPENDS )
set(cpp00-urdf_MEMBER_OF_GROUPS )
set(cpp00-urdf_DEPRECATED "")
set(cpp00-urdf_EXPORT_TAGS)
list(APPEND cpp00-urdf_EXPORT_TAGS "<build_type>ament_cmake</build_type>")
